//
//  TBChromoView.m
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import "TBChromoView.h"
#import "TBChromoDatum.h"
#import "TBChromoModel.h"


@implementation TBChromoView

@synthesize offset;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        offset = 0;
        chromoScale = 50;
        speedFactor = 8000.0;
    }
    return self;
}


-(void)setModel:(TBChromoModel *)aModel{
    model = aModel;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Get the graphics context and clear it
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextClearRect(ctx, rect);
    CGPoint pt;
    
    
    
    CGFloat minX = 0 + offset/speedFactor;
    CGFloat maxX = (1.0/chromoScale) + offset/speedFactor;
    
    if(minX >=1.0){
        offset = 0;
    }
    
    int count = 0;
    CGFloat current = [model fetchDataAt:count].x;
    
    while(minX>current){
        current = [model fetchDataAt:++count].x;
    }
    
    float adjust = + M_PI / 2.0 + M_PI * 0.2;
    
    float angle;
    CGFloat xFactor;
    
    while(maxX>current){
        pt = [model fetchDataAt:count++];
        
        CGFloat r = 180.0 * pt.y;
        xFactor = (pt.x-minX)*chromoScale;
        
        float colorFactor = MIN(1.0,MAX(0.0,(pt.y - 0.5)*5));
        
        //{r:27, g:126, b:224}, {r:224, g:86, b:27}
        
        float r1 = 27.0,g1=126,b1=224;
        float r2 = 224.0,g2=86,b2=27;
        
        float red   = (((1.0-colorFactor) * r1) + (colorFactor * r2))/255.0f;
        float green = (((1.0-colorFactor) * g1) + (colorFactor * g2))/255.0f;
        float blue  = (((1.0-colorFactor) * b1) + (colorFactor * b2))/255.0f;
        
        float alpha = 1.0;
        if (xFactor>0.9) {
            alpha = 1.0 - ((xFactor - 0.9) * 10.0);
        }
        
        if (xFactor<0.1) {
            alpha = xFactor  * 10.0;
        }
        
        CGContextSetRGBFillColor(ctx, red, green, blue,alpha);
        
        angle = M_PI * 2.0 * 0.8 * xFactor + adjust;
        
        CGFloat x = r * cosf(angle) + 160;
        CGFloat y = r * sinf(angle) + 200;
        
        CGContextFillRect(ctx, CGRectMake(x,y, 2, 2));
        current = pt.x;
    }
    /*
    
    //draw triangle
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint   (ctx, 160, 200);
    
    CGContextAddLineToPoint(ctx, 150, 36);
    CGContextAddLineToPoint(ctx, 160, 34);
    CGContextAddLineToPoint(ctx, 170, 36);
    CGContextClosePath(ctx);
    
    CGContextSetRGBFillColor(ctx, 1, 1, 1, 0.5);
    CGContextFillPath(ctx);
    */
    
    // draw lines
    
    for(int i=0;i<[[model lines]count];i++){
        float pos = ([[[model lines]objectAtIndex:i]floatValue])/speedFactor;
        if((pos>=minX)&&(pos<=maxX)){
            //NSLog(@"line %f",pos);
            xFactor = (pos-minX)*chromoScale;
            angle = M_PI * 2.0 * 0.8 * xFactor + adjust;
            
            CGFloat white[4] = {1.0f, 1.0f, 1.0f, 0.75f};
            CGContextSetStrokeColor(ctx, white);
            CGContextSetLineWidth(ctx,3);
            CGContextBeginPath(ctx);
            //CGContextMoveToPoint(ctx, 160.0f, 200.0f);
            
            float r2 = 145.0;
            float r1 = 70.0;
            
            CGFloat x1 = r1 * cosf(angle) + 160;
            CGFloat y1 = r1 * sinf(angle) + 200;
            
            CGFloat x2 = r2 * cosf(angle) + 160;
            CGFloat y2 = r2 * sinf(angle) + 200;
            
            CGContextMoveToPoint(ctx, x1, y1);
            CGContextAddLineToPoint(ctx, x2, y2);
            CGContextStrokePath(ctx);
            
            /*
            CGFloat brightWhite[4] = {1.0f, 1.0f, 1.0f, 1.0f};
            CGContextSetFillColor(ctx, brightWhite);
            CGContextFillEllipseInRect(ctx, CGRectMake(x-5, y-5, 10, 10));
            */
            
            
            
        } /*else {
            NSLog(@"miss p:%f min:%f max:%f",pos,minX,maxX);
        }
           */
    }
}

@end
