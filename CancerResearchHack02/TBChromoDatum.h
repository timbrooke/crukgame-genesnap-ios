//
//  TBChromoDatum.h
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBChromoDatum : NSObject {
    int chromoId;
    double position;
    double freq;
}

@property (readwrite) int chromoId;
@property (readwrite) double position;
@property (readwrite) double freq;

@end
