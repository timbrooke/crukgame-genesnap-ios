//
//  TBChromoModel.h
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBChromoModel : NSObject {
    NSMutableArray *data;
    float maxFreq;
    float minFreq;
    float minPosition;
    float maxPosition;
    NSMutableArray *normalizedData;
    
    NSMutableArray *lines;
}

-(void)loadData:(NSString *)filename;
-(CGPoint)fetchDataAt:(int)index;
-(void)addLine:(float)position;

@property (readonly) float maxFreq;
@property (readonly) float minFreq;
@property (readonly) NSMutableArray *lines;

@end
