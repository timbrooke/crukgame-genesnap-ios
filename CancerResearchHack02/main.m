//
//  main.m
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TBAppDelegate class]));
    }
}
