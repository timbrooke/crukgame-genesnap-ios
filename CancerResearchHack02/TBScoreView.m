//
//  TBScoreView.m
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 03/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import "TBScoreView.h"

@implementation TBScoreView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect frame2 = CGRectMake(0, 10, frame.size.width, 70);
        label = [[UILabel alloc]initWithFrame:frame2];
        [label setTextColor:[UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0]];
        [label setBackgroundColor:[UIColor blackColor]];
        label.textAlignment = UITextAlignmentCenter;
        [label setFont:[UIFont fontWithName:@"Verdana-Bold" size:40]];
        [self addSubview:label];
        [label setText:@"00000000"];
        [self setBackgroundColor:[UIColor blackColor]];
        [self setNeedsDisplay];
        trueScore = 0.0;
        lagScore = 0;
        
    }
    return self;
}

-(void)setScore:(float)num{
    trueScore = num;
}

-(float)score{
    return trueScore;
}

- (void)update{
    if(lagScore < trueScore){
        lagScore += 5;
        if(lagScore>trueScore){
            lagScore = floorf(trueScore);
        }
        NSString *txt = [NSString stringWithFormat:@"%d",lagScore];
        while([txt length]<6){
            txt = [NSString stringWithFormat:@"0%@",txt];
        }
        label.text = txt;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
