//
//  TBScoreView.h
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 03/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBScoreView : UIView {
    UILabel *label;
    float trueScore;
    int lagScore;
}

-(void)setScore:(float)num;
-(float)score;
-(void)update;

@end
