//
//  TBViewController.h
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TBChromoView;
@class TBChromoModel;
@class TBScoreView;

@interface TBViewController : UIViewController {
    TBChromoModel *model;
    TBChromoView *chromoView;
    TBScoreView *scoreView;
    float spinVelocity;
}

//-(void)handleButtonClick;

@end
