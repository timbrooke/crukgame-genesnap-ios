//
//  TBChromoView.h
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TBChromoModel;

@interface TBChromoView : UIView {
    TBChromoModel *model;
    int offset;
    CGFloat chromoScale;
    CGFloat speedFactor;
}

-(void)setModel:(TBChromoModel *)aModel;
@property (readwrite) int offset;



@end
