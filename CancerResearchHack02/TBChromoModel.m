//
//  TBChromoModel.m
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import "TBChromoModel.h"
#import "TBChromoDatum.h"

@implementation TBChromoModel

@synthesize maxFreq,minFreq,lines;

-(id) init {
    if((self = [super init])) {
        lines = [[NSMutableArray alloc]initWithCapacity:30];
    }
    return self;
}

-(void)addLine:(float)position{
    [lines addObject:[NSNumber numberWithFloat:position]];
}

-(CGPoint)fetchDataAt:(int)index{
    if(index < [data count]){
        NSValue *val = [normalizedData objectAtIndex:index];
        if(val!=NULL){
            return [val CGPointValue];
        }
    }
    return CGPointMake(-1,-1);
}

-(void)loadData:(NSString *)filename{
    data = [[NSMutableArray alloc]initWithCapacity:60000];
    normalizedData = [[NSMutableArray alloc]initWithCapacity:60000];
    NSError *err;
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"txt"];
    NSString *contents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&err];
    if(!contents) {
        //handle error
        NSLog(@"Unable to load data file");
    } else {
        NSArray *txtLines = [contents componentsSeparatedByString:@"\n"];
        TBChromoDatum *datum;
        maxFreq = FLT_MIN;
        minFreq = FLT_MAX;
        minPosition = FLT_MAX;
        maxPosition = FLT_MIN;
        for(int i=0;i<[txtLines count];i++){
            NSArray *bits = [[txtLines objectAtIndex:i] componentsSeparatedByString:@"\t"];
            if([bits count] == 4){
                datum = [[TBChromoDatum alloc]init];
                datum.chromoId = [[bits objectAtIndex:0]integerValue];
                datum.position = [[bits objectAtIndex:1]floatValue];
                datum.freq = [[bits objectAtIndex:2]floatValue];
                minFreq = minFreq > datum.freq ? datum.freq : minFreq;
                maxFreq = maxFreq < datum.freq ? datum.freq : maxFreq;
                if(minPosition>datum.position){
                    minPosition = datum.position;
                }
                if(maxPosition<datum.position){
                    maxPosition = datum.position;
                }
                [data addObject:datum];
            }
        }
        CGPoint pt;
        CGFloat posDiff = maxPosition - minPosition;
        CGFloat freqDiff = maxFreq - minFreq;
        for(int i=0;i<[data count];i++){
            datum = [data objectAtIndex:i];
            pt = CGPointMake((datum.position-minPosition)/posDiff,(datum.freq-minFreq)/freqDiff);
            [normalizedData addObject:[NSValue valueWithCGPoint:pt]];
        }
        NSLog(@"Data loaded");
    }
}

@end
