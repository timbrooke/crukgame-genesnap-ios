//
//  TBViewController.m
//  CancerResearchHack02
//
//  Created by Timothy Brooke on 02/03/2013.
//  Copyright (c) 2013 Timothy Brooke. All rights reserved.
//

#import "TBViewController.h"
#import "TBChromoModel.h"
#import "TBChromoView.h"
#import "TBScoreView.h"

@interface TBViewController ()

@end

@implementation TBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    spinVelocity = 1;
    
    model = [[TBChromoModel alloc]init];
    [model loadData:@"S3_Seg_Chrom4"];
    CGRect rect = [[self view]bounds];
    rect.origin.y+=60;
    chromoView = [[TBChromoView alloc]initWithFrame: rect];
    [chromoView setModel:model];
    [[self view]addSubview:chromoView];
    [chromoView setNeedsDisplay];
    
    /*
    UIButton *btnTwo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTwo.frame = CGRectMake(40, 360, 240, 60);
    
    [btnTwo setTitle:@"vc2:v1" forState:UIControlStateNormal];
    [btnTwo addTarget:self action:@selector(handleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *btnImage = [UIImage imageNamed:@"button.fw.png"];
    [btnTwo setImage:btnImage forState:UIControlStateNormal];
    
    [self.view addSubview:btnTwo];
    */
    
    UIButton *fwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fwdButton.frame = CGRectMake(220,380,60,60);
    UIImage *fwdButtonImage = [UIImage imageNamed:@"forwardButton.png"];
    [fwdButton setImage:fwdButtonImage forState:UIControlStateNormal];
    [self.view addSubview:fwdButton];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(40,380,60,60);
    UIImage *backButtonImage = [UIImage imageNamed:@"backButton.png"];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    [self.view addSubview:backButton];

    [backButton addTarget:self action:@selector(handleBackButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [fwdButton addTarget:self action:@selector(handleForwardButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    scoreView = [[TBScoreView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 80)];
    [[self view]addSubview:scoreView];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0/15.0 target:self selector:@selector(animate:) userInfo:nil repeats:YES];
}

-(void)handleBackButtonClick{
    NSLog(@"BACK Q");
    if(spinVelocity < 0){
        spinVelocity *= 2;
    } else {
        spinVelocity = -1;
    }
}


-(void)handleForwardButtonClick{
    NSLog(@"FORWARD Q");
    if(spinVelocity > 0){
        spinVelocity *= 2;
    } else {
        spinVelocity = 1;
    }
}

- (void)animate:(NSTimer *)timer{
    int nupos =[chromoView offset]+spinVelocity;
    if(nupos<0){
        nupos = 0;
    }
    [chromoView setOffset:nupos];
    
    [chromoView setNeedsDisplay];
    //[scoreView setScore:[scoreView score]];
    scoreView.score = scoreView.score + 0.05;
    [scoreView update];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch=[[event allTouches]anyObject];
    CGPoint pt = [touch locationInView:chromoView];
    
   // NSLog(@"touch:%f,%f",pt.x,pt.y);
    CGPoint diff = CGPointMake(pt.x - 160.0, pt.y - 200.0);
    double ang = atan2((double)diff.y, (double)diff.x);
    
    /*
    if(ang>0){
        ang = -M_PI - (M_PI - ang);
    }
    */
    
    ang -= M_PI/2.0;
    
    if(ang>0){
        ang = ang - M_PI*2.0;
    }
    
    float frac = -ang/(M_PI*2.0);
    
    NSLog(@"A:%f F:%f",180.0*ang/M_PI,frac);
    
    
    if(frac>=0.1 && frac<=0.9){
        float trueFrac = (frac-0.1)/0.8;
        float pos = [chromoView offset] + 160 - (160 * trueFrac);
        [model addLine:pos];
    }
    
    scoreView.score += 100;

    
    
    
}




@end
